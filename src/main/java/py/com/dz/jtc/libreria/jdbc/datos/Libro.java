package py.com.dz.jtc.libreria.jdbc.datos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import py.com.dz.jtc.libreria.jdbc.utilitarios.JdbcUtil;
import py.com.dz.jtc.libreria.jdbc.utilitarios.LogUtil;

/**
 * Clase Libro
 * Curso de Programacion Java
 * @author Derlis Zarate
 */
public class Libro {
    
    private Integer codigo;
    private String nombre;
    private String autor;
    private Integer anhoPub;
    private Editorial editorial;

    /**
     * @return the codigo
     */
    public Integer getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the autor
     */
    public String getAutor() {
        return autor;
    }

    /**
     * @param autor the autor to set
     */
    public void setAutor(String autor) {
        this.autor = autor;
    }

    /**
     * @return the anhoPub
     */
    public Integer getAnhoPub() {
        return anhoPub;
    }

    /**
     * @param anhoPub the anhoPub to set
     */
    public void setAnhoPub(Integer anhoPub) {
        this.anhoPub = anhoPub;
    }

    /**
     * @return the editorial
     */
    public Editorial getEditorial() {
        return editorial;
    }

    /**
     * @param editorial the editorial to set
     */
    public void setEditorial(Editorial editorial) {
        this.editorial = editorial;
    }

    /**
     * Retorna la cantidad de registros en tabla Libro o -1 en caso de error
     * @return Nro de registros o -1 en caso de error
     */
    public int getCantidadRegistros() {
        Connection con = JdbcUtil.getInstance().getConnection();
        if (con != null) {
            int nro = -1;
            
            try {
                Statement stm = con.createStatement();
                ResultSet rs = stm.executeQuery("select count(*) from libro");
                if (rs != null) {
                    if (rs.next()) {
                        nro = rs.getInt(1);
                    }
                }
                con.close();
            } catch (Exception e) {
                LogUtil.ERROR("Error al ejecutar consulta", e);
            }
                        
            return nro;
        } else {
            return -1;
        }        
    }

    /**
     * Metodo que retorna el listado completo de Libros de la BD
     * @return Lista de Libros
     */
    public List<Libro> listaLibros() {
        List<Libro> resp = new ArrayList<Libro>();
        
        Connection con = JdbcUtil.getInstance().getConnection();
        if (con != null) {            
            try {
                Statement stm = con.createStatement();
                ResultSet rs = stm.executeQuery("select * from libro");
                if (rs != null) {
                    while (rs.next()) {
                        Libro l = new Libro();
                        l.setCodigo(rs.getInt(1));
                        l.setNombre(rs.getString(2));
                        l.setAutor(rs.getString(3));
                        l.setAnhoPub(rs.getInt(4));                        
                        Editorial e = new Editorial();                        
                        l.setEditorial(e.buscarEditorialPorID(rs.getInt(5)));
                        
                        resp.add(l);
                    }
                }
                con.close();
            } catch (Exception e) {
                LogUtil.ERROR("Error al ejecutar consulta", e);
            }
        }
        
        return resp;
    }
    
    /**
     * Inserta un nuevo registro en tabla Libro
     * @param nombre Nombre del libro
     * @param autor  Autor del libro
     * @param anho Anho de publicacion
     * @param codEd Codigo de la Editorial
     */
    public void insert(String nombre, String autor, int anho, int codEd) {
        Connection con = JdbcUtil.getInstance().getConnection();
        if (con != null) {            
            try {
                PreparedStatement stm = con.prepareStatement("insert into libro (nombre, autor, anhoPub, editorial) values (?,?,?,?)");
                stm.setString(1, nombre);
                stm.setString(2, autor);
                stm.setInt(3, anho);
                stm.setInt(4, codEd);
                
                int rs = stm.executeUpdate();
                if (rs == 1) {
                    System.out.printf("\nLibro %s creado exitosamente", nombre);
                }
                con.commit();
                con.close();
            } catch (Exception e) {
                LogUtil.ERROR("Error al ejecutar consulta", e);
            }
        }
    }

    /**
     * Busca un registro por su ID
     * @param id ID del registro
     * @return Registro con ID o NULL si no existe
     */
    public Libro buscarLibroPorID(int id) {
        Libro li = null;
        Connection con = JdbcUtil.getInstance().getConnection();
        if (con != null) {
            try {
                PreparedStatement stm = con.prepareStatement("select * from libro where codigo = ?");
                stm.setInt(1, id);
                ResultSet rs = stm.executeQuery();
                if (rs != null) {
                    if (rs.next()) {
                        li = new Libro();
                        li.setCodigo(rs.getInt(1));
                        li.setNombre(rs.getString(2));
                        li.setAutor(rs.getString(3));
                        li.setAnhoPub(rs.getInt(4));                        
                        Editorial e = new Editorial();                        
                        li.setEditorial(e.buscarEditorialPorID(rs.getInt(5)));
                    }
                }
                con.close();
            } catch (Exception ex) {
                LogUtil.ERROR("Error al ejecutar consulta", ex);
            }
                        
            return li;
        } else {
            return null;
        }
    }

    /**
     * Elimina el registro con ID recibido
     * @param codigo ID del registro a eliminar
     */
    public void delete(int codigo) {
        Connection con = JdbcUtil.getInstance().getConnection();
        if (con != null) {            
            try {
                PreparedStatement stm = con.prepareStatement("delete from libro where codigo = ?");
                stm.setInt(1, codigo);
                
                int rs = stm.executeUpdate();
                if (rs == 1) {
                    System.out.printf("\nLibro eliminado exitosamente");
                }
                con.close();
            } catch (Exception e) {
                LogUtil.ERROR("Error al ejecutar consulta", e);
            }
        }
    }
    
} //Fin de clase Libro
