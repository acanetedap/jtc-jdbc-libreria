package py.com.dz.jtc.libreria.jdbc.libreria;

import py.com.dz.jtc.libreria.jdbc.datos.Editorial;
import py.com.dz.jtc.libreria.jdbc.datos.Libro;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import py.com.dz.jtc.libreria.jdbc.utilitarios.LogUtil;

/**
 * Clase GestorDeLibreria
 * Curso de Programacion Java
 * @author Derlis Zarate
 */
public class GestorDeLibreria {

    /**
     * Metodo main de la clase
     * @param args Argumentos de linea de comandos
     */
    public static void main(String[] args) {
        int opcionUsuario = 0;
        
        LogUtil.INFO("Sistema GestorDeLibreria iniciado!");
        
        do {
            opcionUsuario = mostrarMenuPrincipal();
            
            switch(opcionUsuario) {
                case 1:
                    LogUtil.INFO("Acceso a Listado de Editoriales");
                    privListarEditoriales();
                    mostrarPausa();
                    break;
                case 2:
                    LogUtil.INFO("Acceso a Agregar Editorial");
                    privAgregarEditorial();
                    mostrarPausa();
                    break;
                case 3:
                    LogUtil.INFO("Acceso a Listado de Libros");
                    privListarLibros();
                    mostrarPausa();
                    break;
                case 4:
                    LogUtil.INFO("Acceso a Agregar Libro");
                    privAgregarLibro();
                    mostrarPausa();
                    break;
                case 5:
                    LogUtil.INFO("Acceso a Eliminar Libro");
                    privEliminarLibro();
                    mostrarPausa();
                    break;
                case 6:
                    LogUtil.INFO("Acceso a Salir del Sistema");
                    break;
            }
            
        } while (opcionUsuario != 6);
        
        LogUtil.INFO("Sistema GestorDeLibreria finalizado!");

    } //Fin de main
    
    /**
     * Metodo que simula el limpiado de pantalla en consola, imprimiendo 40 saltos de linea
     */
    private static void limpiarPantalla() {
        for (int i = 0; i < 40; i++) {
            System.out.printf("\n");
        }
    }
    
    /**
     * Metodo auxiliar que simula una pausa para el usuario
     */
    private static void mostrarPausa() {
        System.out.printf("\nPresione la tecla ENTER para volver al Menu Principal...");
        Scanner s = new Scanner(System.in);
        String val = s.nextLine();
    }
    
    /**
     * Metodo que muestra el menu principal de la aplicacion en pantalla y 
     * retorna el codigo numerico de la opcion elegida por el usuario
     * @return Codigo numerico de opcion elegida por el usuario
     */
    public static int mostrarMenuPrincipal() {
        int resp = 0;
        String msgAux = "";
        
        do {
            limpiarPantalla();
            System.out.printf("\n\t========================");
            System.out.printf("\n\t  GESTION DE LIBRERIAS  ");
            System.out.printf("\n\t========================");
            System.out.printf("\n\t     Menu Principal     ");
            System.out.printf("\n                          ");
            System.out.printf("\n1. Listar Editoriales");
            System.out.printf("\n2. Agregar Nueva Editorial");
            System.out.printf("\n3. Listar Libros");
            System.out.printf("\n4. Agregar Nuevo Libro");
            System.out.printf("\n5. Eliminar Libro Existente");
            System.out.printf("\n6. SALIR del sistema");
            System.out.printf("\n                          ");
            
            if (msgAux.trim().length() > 0) {
                System.out.printf("\n\t%s\n", msgAux);
            }
            
            System.out.printf("\nIngrese nro. de opcion: ");
            
            Scanner scan = new Scanner(System.in);
            try {
                resp = scan.nextInt();
            } catch (Exception e) {
                LogUtil.ERROR("Error al leer opcion del usuario", e);
                resp = 0;
            }
            
            if (resp <= 0 || resp > 6) {
                msgAux = "Opcion seleccionada no valida. Favor ingrese un nro. del 1 al 6.";
            }
            
        } while (resp <= 0 || resp > 6);
        
        return resp;
    }

    /**
     * Ejecuta opcion 1 - Impresion de listado de editoriales en pantalla
     */
    private static void privListarEditoriales() {
        limpiarPantalla();
        System.out.printf(" LISTADO DE EDITORIALES ");
        System.out.printf("\n========================");
        
        Editorial e = new Editorial();
        int canReg = e.getCantidadRegistros();

        if (canReg > 0) {
            List<Editorial> lista = e.listaEditoriales();
            if (lista != null) {
                System.out.printf("\n%5s%20s", "Cod.", "Nombre");
                Iterator<Editorial> it = lista.iterator();
                while (it.hasNext()) {
                    Editorial ed = it.next();
                    System.out.printf("\n%5d%20s", ed.getCodEd(), ed.getNombre());
                }
            }
        }
        
        System.out.printf("\n\nTotal de registros %d.\nFin del listado.", canReg);
    }    

    /**
     * Ejecuta opcion 2 - Agregar nueva Editorial
     */
    private static void privAgregarEditorial() {
        limpiarPantalla();
        System.out.printf(" AGREGAR NUEVA EDITORIAL ");
        System.out.printf("\n=========================");
       
        Scanner s = new Scanner(System.in);
        String val = "";
        do {
            System.out.printf("\n\tIngrese nombre: ");
            val = s.nextLine();
        } while (val.trim().length() == 0);

        //Seteamos todos los campos menos la clave del objeto
        //pues es un autonumerico, y al ejecutar el insert se asignara el valor automaticamente
        Editorial ed = new Editorial();
        ed.insert(val);        
    }
    
    /**
     * Ejecuta opcion 3 - Impresion de listado de libros en pantalla
     */
    private static void privListarLibros() {
        limpiarPantalla();
        System.out.printf(" LISTADO DE LIBROS ");
        System.out.printf("\n===================");
        
        Libro li = new Libro();
        int canReg = li.getCantidadRegistros();

        if (canReg > 0) {
        	//TODO: JDBC: usando el objeto li, invocar a la funcion listaLibros()
        	//TODO: JDBC: si el resultado de la invocacion no es un objeto null, iterar sus elementos e imprimirlos en pantalla
            
        }
        
        System.out.printf("\n\nTotal de registros %d.\nFin del listado.", canReg);
    }
    
    /**
     * Ejecuta opcion 4 - Agregar nuevo Libro
     */
    private static void privAgregarLibro() {
        limpiarPantalla();
        System.out.printf(" AGREGAR NUEVO LIBRO ");
        System.out.printf("\n=====================");
       
        Scanner s = new Scanner(System.in);
                
        //Seteamos todos los campos menos la clave del objeto
        //pues es un autonumerico, y al ejecutar el insert se asignara el valor automaticamente
        Libro li = new Libro();
        
        String nombre = "";
        do {
            System.out.printf("\n\tIngrese nombre libro: ");
            nombre = s.nextLine();
        } while (nombre.trim().length() == 0);
        
        String autor = "";
        do {
            System.out.printf("\n\tIngrese nombre autor: ");
            autor = s.nextLine();
        } while (autor.trim().length() == 0);
        
        int anho = 0;
        do {
            System.out.printf("\n\tIngrese anho publicacion (> 0): ");
            try {
                anho = s.nextInt();
            } catch (Exception e) {
                LogUtil.ERROR("Error al leer anho de publicacion", e);
                anho = 0;
            }
        } while (anho <= 0);
        
        int codEd = 0;
        do {
            System.out.printf("\n\tIngrese codigo de Editorial (> 0): ");
            try {
                codEd = s.nextInt();
            } catch (Exception e) {
                LogUtil.ERROR("Error al leer codigo de Editorial", e);
                codEd = 0;
            }
        } while (codEd <= 0);        
        
        //al salir del while anterior, el usuario ingreso un cod editorial mayor a 0
        //buscamos si existe una editorial con ese codigo
        Editorial ed = new Editorial();
        if (ed.buscarEditorialPorID(codEd) == null) {
            System.out.printf("El codigo de editorial no existe. No se puede crear el Libro");
            return;
        } else {
            //TODO: JDBC: Genere el codigo faltante para invocar a la funcion de insercion 
            //sobre el objeto li, usando los parametros ingresados por el usuario
        }        
    }

    /**
     * Ejecuta opcion 5 - Eliminar Libro
     */
    private static void privEliminarLibro() {
        limpiarPantalla();
        System.out.printf(" ELIMINAR LIBRO ");
        System.out.printf("\n================");
       
        Scanner s = new Scanner(System.in);
                
        int ival = 0;
        do {
            System.out.printf("\n\tIngrese codigo del libro a eliminar (> 0): ");
            try {
                ival = s.nextInt();
            } catch (Exception e) {
                LogUtil.ERROR("Error al leer codigo del libro", e);
                ival = 0;
            }
        } while (ival <= 0);        
        
        //al salir del while anterior, el usuario ingreso un cod libro mayor a 0
        //buscamos si existe un libro con ese codigo
        Libro li = new Libro();
        Libro resultadoBusqueda = li.buscarLibroPorID(ival);
        if (resultadoBusqueda == null) {
            System.out.printf("El codigo del libro no existe. No se puede eliminar el Libro");
            return;
        } else {
            System.out.printf("\nLibro a eliminar...");
            System.out.printf("\n%-4s%-30s%-20s%-15s%-5s", "Cod", "Nombre", "Autor", "Editorial", "Anho");                
            System.out.printf("\n%-4s%-30s%-20s%-15s%-5s", 
                              resultadoBusqueda.getCodigo(), 
                              resultadoBusqueda.getNombre().substring(0, Math.min(resultadoBusqueda.getNombre().length(), 29)), 
                              resultadoBusqueda.getAutor().substring(0, Math.min(resultadoBusqueda.getAutor().length(), 19)), 
                              resultadoBusqueda.getEditorial().getNombre().substring(0, Math.min(resultadoBusqueda.getEditorial().getNombre().length(), 14)),
                              resultadoBusqueda.getAnhoPub());
            
            System.out.printf("\n\nEsta seguro de eliminar el registro? (S/N) ");
            String val = s.next();
            if (val.trim().equalsIgnoreCase("S")) {
                li.delete(resultadoBusqueda.getCodigo());
            } else {
                System.out.printf("\nEliminacion cancelada.");
            }
        }                
    }

} //Fin de clase GestorDeLibreria
