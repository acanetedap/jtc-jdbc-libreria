package py.com.dz.jtc.libreria.jdbc.datos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import py.com.dz.jtc.libreria.jdbc.utilitarios.JdbcUtil;
import py.com.dz.jtc.libreria.jdbc.utilitarios.LogUtil;

/**
 * Clase Editorial
 * Curso de Programacion Java
 * @author Derlis Zarate 
 */
public class Editorial {

    private Integer codEd;
    private String nombre;
    private List<Libro> libroList;

    /**
     * @return the codEd
     */
    public Integer getCodEd() {
        return codEd;
    }

    /**
     * @param codEd the codEd to set
     */
    public void setCodEd(Integer codEd) {
        this.codEd = codEd;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the libroList
     */
    public List<Libro> getLibroList() {
        return libroList;
    }

    /**
     * @param libroList the libroList to set
     */
    public void setLibroList(List<Libro> libroList) {
        this.libroList = libroList;
    }    
    
    /**
     * Retorna la cantidad de registros en tabla Editorial o -1 en caso de error
     * @return Nro de registros o -1 en caso de error
     */
    public int getCantidadRegistros() {
        Connection con = JdbcUtil.getInstance().getConnection();
        if (con != null) {
            int nro = -1;
            
            try {
                Statement stm = con.createStatement();
                ResultSet rs = stm.executeQuery("select count(*) from editorial");
                if (rs != null) {
                    if (rs.next()) {
                        nro = rs.getInt(1);
                    }
                }
                con.close();
            } catch (Exception e) {
                LogUtil.ERROR("Error al ejecutar consulta", e);
            }
                        
            return nro;
        } else {
            return -1;
        }        
    }

    /**
     * Metodo que retorna el listado completo de Editoriales de la BD
     * @return Lista de Editoriales
     */
    public List<Editorial> listaEditoriales() {
        List<Editorial> resp = new ArrayList<Editorial>();
        
        Connection con = JdbcUtil.getInstance().getConnection();
        if (con != null) {            
            try {
                Statement stm = con.createStatement();
                ResultSet rs = stm.executeQuery("select * from editorial");
                if (rs != null) {
                    while (rs.next()) {
                        Editorial e = new Editorial();
                        e.setCodEd(rs.getInt(1));
                        e.setNombre(rs.getString(2));
                        
                        resp.add(e);
                    }
                }
                con.close();
            } catch (Exception e) {
                LogUtil.ERROR("Error al ejecutar consulta", e);
            }
        }
        
        return resp;
    }
    
    /**
     * Inserta un nuevo registro en tabla Editorial
     * @param nombre Nombre de la Editorial a crear
     */
    public void insert(String nombre) {
        Connection con = JdbcUtil.getInstance().getConnection();
        if (con != null) {            
            try {
                PreparedStatement stm = con.prepareStatement("insert into editorial (nombre) values (?)");
                stm.setString(1, nombre);
                int rs = stm.executeUpdate();
                if (rs == 1) {
                    System.out.printf("\nEditorial %s creada exitosamente", nombre);
                }
                con.close();
            } catch (Exception e) {
                LogUtil.ERROR("Error al ejecutar consulta", e);
            }
        }
    }

    /**
     * Busca un registro por su ID
     * @param id ID del registro
     * @return Registro con ID o NULL si no existe
     */
    public Editorial buscarEditorialPorID(int id) {
        Editorial e = null;
        Connection con = JdbcUtil.getInstance().getConnection();
        if (con != null) {
            try {
                PreparedStatement stm = con.prepareStatement("select * from editorial where codEd = ?");
                stm.setInt(1, id);
                ResultSet rs = stm.executeQuery();
                if (rs != null) {
                    if (rs.next()) {
                        e = new Editorial();
                        e.setCodEd(rs.getInt(1));
                        e.setNombre(rs.getString(2));
                    }
                } 
                con.close();
            } catch (Exception ex) {
                LogUtil.ERROR("Error al ejecutar consulta", ex);
            }
                        
            return e;
        } else {
            return null;
        }
    }
    
} //Fin de clase Editorial